function [ fitness, F, eAPA ] = runAPA ( pop, Lx, x, d, w0, type )
% RunAPA - Fitness funtion for the genetic algorithm
%
%   Inputs:
%   - pop: population of the GA
%   - Lx: length of the signal to consider
%   - x: signal
%   - d: desired signal
%   - w0: optimal inpulse response
%   - type: 'MISA' computes the misadjustment, 'HUMAN' asks the user for a
%   rating
%
%   Outputs:
%   - fitness: vector of fitness evaluations
%   - F: struct resulting from the filtering operation
%   - eAPA: error of the APA filter

fitness = zeros(1, size(pop,1));

for i=1:size(pop, 1)

    hyp = pop(i, :);
    
    mu = hyp(1);
    K = ceil(hyp(2));
    delta = hyp(3);
    M = ceil(hyp(4));
    
    optimal_m = max(length(w0), M);
    Aw0 = zeros(optimal_m,1);
    Aw0(1:length(w0)) = w0;
    Aw = zeros(optimal_m,1);
    
    wi = 0;
    
    eAPA = zeros(Lx,1);
    yAPA = zeros(Lx,1);
    
    xBuff = zeros(K,M);                                                                     % Input buffer matrix inizialization
    dBuff = zeros(K,1);                                                                     % Desired signal buffer initialization
    dI = delta*eye(K);                                                                      % Regularization matrix inizialization
    w = wi*ones(M,1);                                                                       % Filter vector inizialization
    
    for n = 1:Lx
        
        %----------Update input and desired signal buffers----------------------------------%
        xBuff(1:K-1,:) = xBuff(2:K,:);                                              % Shift covarince matrix X down (eqn 4.43)
        xBuff(K,2:M) = xBuff(K,1:M-1);                                          % Shift current buffer down
        xBuff(K,1) = x(n);                                                                 % Assign current input signal sample (eqn 4.43)
        dBuff(1:K-1) = dBuff(2:K);                                                  % Shift desired signal buffer down
        dBuff(K) = d(n);                                                                   % Assign current desired signal sample
        
        %----------Compute output and residual signals--------------------------------------%
        yBuff = xBuff*w;                                                                % Compute current output signal buffer (K,1) = (K,M) x (M,1)
        yAPA(n) = yBuff(K);                                                                     % Assign current output sample
        eBuff = dBuff - yBuff;                                                            % Compute current error signal buffer (K,1) = (K,1) - (K,1)
        eAPA(n) = eBuff(K);                                                                     % Assign current error sample
        
        %----------Compute filter coefficients update---------------------------------------%
        w = w + mu*(xBuff'/(dI + xBuff*xBuff'))*eBuff;                        % Filter update: (M,1) = (M,1) + (M,K) x (K,1) (eqn 6.42)
        
    end
    
    if(strcmp(type, 'MISA'))
        Aw(1:M) = w;
        fitness(i) = 20*log10(norm((Aw0 - Aw),2)./norm(Aw0,2));
    end
    
    if(strcmp(type, 'HUMAN'))
        playsound(eAPA, 8000, 16000);
        response = menu_center('Rate the clearness of the sound', ...
            'Perfect','Very Good','Good','Average', 'Bad', 'Very Bad');
        refresh(main);
        fitness(i) = response - 1;
    end

end

F = struct('M', M, 'mu', mu, 'K', K, 'dI', dI, 'xBuff', xBuff,...
           'dBuff', dBuff, 'w', w);  

end

