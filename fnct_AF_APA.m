function [e, y, F] = fnct_AF_APA(x, d, F)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ON-LINE FUNCTION: AFFINE PROJECTION ALGORITHM (APA) ADAPTIVE FILTER.
%
% 21 September 2009
% 26 November 2012
%
% Danilo Comminiello
%
% Department of Information, Electronic and Telecommunication Engineering 
% 'Sapienza' University of Rome
% Via Eudossiana,18, I-00184 Roma, Italy
%
%
% INPUT PARAMETERS:
%   x: input signal sample at n-th time instant
%   d: desired signal sample at n-th time instant
%   F.M: filter length
%   F.mu: step size
%   F.K: projection order
%   F.dI: regularization matrix
%   F.xBuff: input buffer vector at time instant n-1 [K,M]
%   F.dBuff: desired signal buffer vector at time instant n-1 [K,1]
%   F.w: filter vector at time instant n-1 [M,1]
%
% OUTPUT PARAMETERS:
%   e: error signal sample at n-th time instant
%   y: output signal sample at n-th time instant
%   F.M: filter length
%   F.mu: step size
%   F.K: projection order
%   F.dI: regularization matrix
%   F.xBuff: input buffer vector at time instant n [K,M]
%   F.dBuff: desired signal buffer vector at time instant n [K,1]
%   F.w: filter vector at time instant n [M,1]
%
%
% REFERENCES: 
%   [1] K. Ozeki and T. Umeda, "An adaptive filtering algorithm using an
%       orthogonal projection to an affine subspace and its properties",
%       Electronics and Communications in Japan, vol. 67-A, no. 5, 19-27,
%       May 1984.
%   [2] A. Uncini, "Elaborazione Adattativa dei Segnali", Aracne Editrice,
%       ISBN: 978:88-548-3142-I, 2010.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%----------Update input and desired signal buffers----------------------------------%
F.xBuff(1:F.K-1,:) = F.xBuff(2:F.K,:);                                              % Shift covarince matrix X down (eqn 4.43)
F.xBuff(F.K,2:F.M) = F.xBuff(F.K,1:F.M-1);                                          % Shift current buffer down
F.xBuff(F.K,1) = x;                                                                 % Assign current input signal sample (eqn 4.43)
F.dBuff(1:F.K-1) = F.dBuff(2:F.K);                                                  % Shift desired signal buffer down
F.dBuff(F.K) = d;                                                                   % Assign current desired signal sample

%----------Compute output and residual signals--------------------------------------%
yBuff = F.xBuff*F.w;                                                                % Compute current output signal buffer (K,1) = (K,M) x (M,1)
y = yBuff(F.K);                                                                     % Assign current output sample
eBuff = F.dBuff - yBuff;                                                            % Compute current error signal buffer (K,1) = (K,1) - (K,1)
e = eBuff(F.K);                                                                     % Assign current error sample
    
%----------Compute filter coefficients update---------------------------------------%
F.w = F.w + F.mu*(F.xBuff'/(F.dI + F.xBuff*F.xBuff'))*eBuff;                        % Filter update: (M,1) = (M,1) + (M,K) x (K,1) (eqn 6.42)