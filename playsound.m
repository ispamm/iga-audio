function player = playsound( x, bitRate, Fs, stringToDisplay, startValue, endValue )
% PLAYSOUND - Play a sound in the backgroun
%   Inputs:
%       - x: the signal to play
%       - bitRate: bitrate of encoding (default 8 KHz)
%       - Fs: sampling frequency
%       - stringToDisplay: string to show to the user for asking the rating

if(nargin < 2)
    bitRate = 8000;
end

if(nargin < 3)
    Fs = 16000;
end

if(nargin < 4)
    stringToDisplay = 'Now playing the sound...';
end

if(nargin < 5)
    startValue = 1;
end

if(nargin < 6)
    endValue = length(x);
end

player = audioplayer(x(startValue:endValue), bitRate);
player.play();

h = waitbar(0, stringToDisplay);
set(h, 'WindowStyle', 'modal')

while (strcmp(player.Running, 'on'))
    cur = player.CurrentSample / player.TotalSamples;
    if(cur < 0.01) 
        cur = 1; 
    end
    waitbar(cur);
end
delete(h);
    
end

