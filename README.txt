
            Interactive GA for Acoustic Echo Cancellation

Description
-------
This code can be used for evolving the optimal parameters of an APA-based
acoustic echo canceller, employing an interactive genetic algorithm (IGA). 
It was used in the following publications:

[1] Comminiello, D., Scardapane, S., Scarpiniti, M., & Uncini, A. (2013, May). 
    User-Driven Quality Enhancement for Audio Signal Processing. 
    In Audio Engineering Society Convention 134. Audio Engineering Society.

[2] Comminiello, D., Scardapane, S., Scarpiniti, M., & Uncini, A. (2013, July). 
    Interactive quality enhancement in acoustic echo cancellation. 
    In 2013 36th International Conference on Telecommunications and 
    Signal Processing (TSP),  (pp. 488-492). IEEE.


Usage 
-------
You can launch the IGA with the main.m script. When the IGA is over, the
program will automatically launch another GA using a standard optimization
function based on the misalignment. At the end, both settings for the echo
canceller (human-based and misaligment-based) are saved in the 'results'
folder.

You can change most on the setting in the 'start_button_Callback' of the
main.m file. In particular, you can change the scenario by modifying lines
93-95. We provide five default scenarios in the 'sounds' folder (see README
file in the folder for a description of the scenarios).

Once you have completed the IGA executions, you can run a subjective 
comparison of the results using the 'randomSound.m' routine. Remember to
modify line 7 by listing all the available results. We provide one example
of results in the folder to test the script.


Other details
-------
The fitness function for the GA (both human-based and misaligment-based)
is implemented in the 'runAPA.m' function. Code of the APA filter is in the
'create_AF_APA' and 'fnct_AF_APA' functions.


Citation
-------
If you use this code or any derivatives thereof in your research, we would
appreciate if you cite at least one of the original papers:

@inproceedings{comminiello2013interactive,
  title={Interactive quality enhancement in acoustic echo cancellation},
  author={Comminiello, Danilo and Scardapane, Simone and Scarpiniti, Michele and Uncini, Aurelio},
  booktitle={2013 36th International Conference on Telecommunications and Signal Processing (TSP)},
  pages={488--492},
  year={2013},
  organization={IEEE}
},

@inproceedings{comminiello2013user,
  title={User-Driven Quality Enhancement for Audio Signal Processing},
  author={Comminiello, Danilo and Scardapane, Simone and Scarpiniti, Michele and Uncini, Aurelio},
  booktitle={Audio Engineering Society Convention 134},
  year={2013},
  organization={Audio Engineering Society}
}.


Licensing
---------
The code is distributed under BSD-2 license. Please see the file called LICENSE.


Contacts
--------

   o If you have any request, bug report, or inquiry, you can contact
     the author at simone [dot] scardapane [at] uniroma1 [dot] it.
   o Additional contact information can also be found on the website of
     the author:
	      http://ispac.ing.uniroma1.it/scardapane/