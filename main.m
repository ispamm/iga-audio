function varargout = main(varargin)
% MAIN MATLAB code for main.fig
%      MAIN, by itself, creates a new MAIN or raises the existing
%      singleton*.
%
%      H = MAIN returns the handle to a new MAIN or the handle to
%      the existing singleton*.
%
%      MAIN('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAIN.M with the given input arguments.
%
%      MAIN('Property','Value',...) creates a new MAIN or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before main_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to main_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help main

% Last Modified by GUIDE v2.5 30-Nov-2012 11:00:18

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @main_OpeningFcn, ...
                   'gui_OutputFcn',  @main_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before main is made visible.
function main_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to main (see VARARGIN)

% Choose default command line output for main
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
movegui(hObject,'center') ;
% UIWAIT makes main wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = main_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in start_button.
function start_button_Callback(hObject, eventdata, handles)
% hObject    handle to start_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.start_button, 'Visible', 'off');
set(handles.explanation_text, 'Visible', 'off');
set(handles.name_text, 'Visible', 'off');
set(handles.name_input, 'Visible', 'off');

set(handles.wait_text, 'Visible', 'on');
refresh(main);

Lx = 60000;

% Change scenario here (see folder 'sounds')
w0 = wavread('sounds/Roomsim150ms_trunc300samples_8kHz.wav');
x = wavread('sounds/Test5_x.wav', Lx);                                                        
d = wavread('sounds/Test5_d.wav',Lx);                                                                                      

% Fitness functions (human-based and classical)
fit = @(y)runAPA(y, Lx, x, d, w0, 'HUMAN');
fit_MISA = @(y)runAPA(y, Lx, x, d, w0, 'MISA');

% Rune the Interactive GA
options = gaoptimset('EliteCount', 1, 'PopulationSize', 5, 'Generations', 10, 'StallGenLimit', 2, 'FitnessLimit', 0, 'Vectorized', 'on');
[bestAPAParameters, fval] = ga(fit, 4, [], [], [], [], [0.01 1 0 200], [2 10 10 1000], [], [2 4], options);

% Final training
[fval bestAPA eBestAPA] = runAPA(bestAPAParameters, Lx, x, d, w0, 'MISA');

set(handles.wait_text, 'String', 'The program has found the perfect setting!');
refresh(main);

% Run the classical GA and perform the final training
options = gaoptimset('EliteCount', 1, 'PopulationSize', 5, 'Generations', 10, 'StallGenLimit', 2, 'Vectorized', 'on', 'Display', 'iter');
[bestAPAParameters_MISA, fval] = ga(fit_MISA, 4, [], [], [], [], [0.01 1 0 200], [2 10 10 1000], [], [2 4], options);
[fval bestAPA eBestAPA_MISA] = runAPA(bestAPAParameters_MISA, Lx, x, d, w0, 'MISA');

% Save the parameters
save(getFileName(handles), 'bestAPAParameters', 'eBestAPA', 'bestAPAParameters_MISA', 'eBestAPA_MISA');


function name_input_Callback(hObject, eventdata, handles)
% hObject    handle to name_input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of name_input as text
%        str2double(get(hObject,'String')) returns contents of name_input as a double


% --- Executes during object creation, after setting all properties.
function name_input_CreateFcn(hObject, eventdata, handles)
% hObject    handle to name_input (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function filename = getFileName(handles)
    name = get(handles.name_input, 'String');
    filename = ['results/test_', name, '_', datestr(now, 'yyyy_mm_dd_HH_MM_SS'), '.mat'];
