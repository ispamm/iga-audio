% Script for selecting a random sound resulting from the IGA routine, and
% asking the user whether it is better than the one originating from the
% classical GA.

% Select a random file
rng shuffle;
files = {'results/test_Test1_2012_12_07_14_32_21.mat'};
j = randi(length(files));
load(files{j}, '-mat');

% Randomizes the first sound to be heard
i = randi(2);
if(i == 1)
    player = playsound(eBestAPA);
    while (strcmp(player.Running, 'on'))
    end
    pause(1);
    playsound(eBestAPA_MISA);
    
else
    player = playsound(eBestAPA_MISA);
    while (strcmp(player.Running, 'on'))
    end
    pause(1);
    playsound(eBestAPA);
end

% Ask the user
input = questdlg('Quale suono era preferibile?', 'Preferenza', 'Primo','Secondo','Non so', 'Non so');

% Show the results
disp(['Il file era: ', files{j}]);

switch input
    case 'Primo'
        if(i == 1)
            disp('Il suono era: HUMAN-DRIVEN');
        else
            disp('Il suono era: MISA-DRIVEN');
        end
    case 'Secondo'
        if(i == 1)
            disp('Il suono era: MISA-DRIVEN');
        else
            disp('Il suono era: HUMAN-DRIVEN');
        end
end