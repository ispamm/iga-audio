%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SCENARIO #1: assenza di contributo vocale near-end
% Segnale far-end: voce femminile - Test1_x
% Segnale near-end: rumore Gaussian bianco con SNR=20dB - Test1_ne
% Segnale microfonico: near-end + (far-end convoluto con risposta impulsiva) - Test1_d


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SCENARIO #2: presenza di contributo vocale near-end continuo
% Segnale far-end: voce femminile - Test2_x
% Segnale near-end: voce maschile - Test2_ne
% Segnale microfonico: near-end + (far-end convoluto con risposta impulsiva) - Test2_d


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SCENARIO #3: presenza di contributo vocale near-end sparso
% Segnale far-end: voce femminile - Test3_x
% Segnale near-end: voce maschile + rumore Gaussian bianco con SNR=30dB - Test3_ne
% Segnale microfonico: near-end + (far-end convoluto con risposta impulsiva) - Test3_d


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SCENARIO #4: presenza di contributo musicale near-end
% Segnale far-end: voce femminile - Test4_x
% Segnale near-end: radio - Test4_ne
% Segnale microfonico: near-end + (far-end convoluto con risposta impulsiva) - Test4_d


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SCENARIO #5: presenza di contributo musicale near-end + voce maschile near end
% Segnale far-end: voce maschile - Test5_x
% Segnale near-end: radio + voce femminile sparsa - Test5_ne
% Segnale microfonico: near-end + (far-end convoluto con risposta impulsiva) - Test5_d
