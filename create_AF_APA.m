function F = create_AF_APA(M, mu, K, delta, wi)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE STRUCTURE: AFFINE PROJECTION ALGORITHM (APA) ADAPTIVE FILTER.
%
% 21 September 2009
% 26 November 2012
%
% Danilo Comminiello
%
% Department of Information, Electronic and Telecommunication Engineering 
% 'Sapienza' University of Rome
% Via Eudossiana,18, I-00184 Roma, Italy
%
%
% INPUT PARAMETERS:
%   M: filter length
%   mu: step size
%   K: projection order
%   delta: regularization factor
%   wi: filter coefficient initialization
%
% OUTPUT PARAMETERS:
%   F.M: filter length
%   F.mu: step size
%   F.K: projection order
%   F.dI: regularization matrix
%   F.xBuff: input buffer vector at time instant n=0 [K,M]
%   F.dBuff: desired signal buffer vector at time instant n=0 [K,1]
%   F.w: filter vector at time instant n=0 [M,1]
%
%
% REFERENCES: 
%   [1] K. Ozeki and T. Umeda, "An adaptive filtering algorithm using an
%       orthogonal projection to an affine subspace and its properties",
%       Electronics and Communications in Japan, vol. 67-A, no. 5, 19-27,
%       May 1984.
%   [2] A. Uncini, "Elaborazione Adattativa dei Segnali", Aracne Editrice,
%       ISBN: 978:88-548-3142-I, 2010.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin < 5
    wi = 0;                                                                             % Initial value for the filter coefficients
end
if nargin < 4
    delta = 10e-3;                                                                      % Default value for the regularization parameter
end
if nargin < 3
    K = 3;                                                                              % Default value for the projection order
end
xBuff = zeros(K,M);                                                                     % Input buffer matrix inizialization
dBuff = zeros(K,1);                                                                     % Desired signal buffer initialization
dI = delta*eye(K);                                                                      % Regularization matrix inizialization
w = wi*ones(M,1);                                                                       % Filter vector inizialization

F = struct('M', M, 'mu', mu, 'K', K, 'dI', dI, 'xBuff', xBuff,...
           'dBuff', dBuff, 'w', w);                                                     % Structure of the NLMS adaptive filter